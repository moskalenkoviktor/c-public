#!/bin/bash

# Run from build directory

set -e

if [ "$#" -lt 1 ]; then
    echo "Usage: $0 task-name"
    exit 1
fi

if [ ! -f compile_commands.json ]; then
    echo "Run this script from the build directory"
    exit 1
fi

cd ..
RESULT=$(python get_block.py $1)
cd build

BLOCK_NAME=$(echo $RESULT | cut -d ' ' -f 1)
INDEX=$(echo $RESULT | cut -d ' ' -f 2)

if [ $INDEX -lt 3 ]; then
    echo -e '\033[1;32mLINTER IS SUCCESS\033[1;32m'
    exit 0
fi

TASK_PATH=../$BLOCK_NAME/$1
FILES=$(find $TASK_PATH -name '*.[ch]' ! -name test.c)

../run-clang-format.py --clang-format-executable clang-format $FILES
clang-tidy -p . $FILES
echo -e '\033[1;32mLINTER IS SUCCESS\033[1;32m'
