#include "acutest.h"
#include "basic-numbers.h"


void TestReplace() {
    TEST_ASSERT(IndexReplace(1, 2, 3) == 1);
    TEST_ASSERT(IndexReplace(123, 0, 1) == 123);
    TEST_ASSERT(IndexReplace(12333, 3, 0) == 12000);
    TEST_ASSERT(IndexReplace(1, 1, 5) == 5);
    TEST_ASSERT(IndexReplace(1111, 1, 0) == 0);
    TEST_ASSERT(IndexReplace(101010, 0, 1) == 111111);
    TEST_ASSERT(IndexReplace(82648232, 2, 9) == 89648939);
    TEST_ASSERT(IndexReplace(0, 0, 1) == 1);
    TEST_ASSERT(IndexReplace(0, 9, 1) == 0);
    TEST_ASSERT(IndexReplace(1337, 3, 7) == 1777);
    TEST_ASSERT(IndexReplace(12222, 2, 0) == 10000);
    TEST_ASSERT(IndexReplace(12345678, 9, 1) == 12345678);
}


void TestReverse() {
    TEST_ASSERT(IndexesReverse(1234, 0) == 1432);
    TEST_ASSERT(IndexesReverse(123456, 3) == 321465);
    TEST_ASSERT(IndexesReverse(1337, 20) == 7331);
    TEST_ASSERT(IndexesReverse(23423, -1) == 32432);
    TEST_ASSERT(IndexesReverse(2423434, 4) == 3242443);
    TEST_ASSERT(IndexesReverse(123456789, 5) == 543216987);
    TEST_ASSERT(IndexesReverse(1, 42) == 1);
    TEST_ASSERT(IndexesReverse(42, 0) == 42);
    TEST_ASSERT(IndexesReverse(42, 1) == 42);
    TEST_ASSERT(IndexesReverse(1, 0) == 1);
}

void TestRemove() {
    TEST_ASSERT(Remove(1234567, 102) == 4567);
    TEST_ASSERT(Remove(13371337, 3201) == 1337);
    TEST_ASSERT(Remove(13371337, 1230) == 1337);
    TEST_ASSERT(Remove(12, 0) == 2);
    TEST_ASSERT(Remove(1, 0) == 0);
    TEST_ASSERT(Remove(42, 10) == 0);
    TEST_ASSERT(Remove(1337, 9) == 1337);
    TEST_ASSERT(Remove(42, 56789) == 42);
    TEST_ASSERT(Remove(123456789, 2468) == 12468);
}


TEST_LIST = {
    {"replace", TestReplace},
    {"reverse", TestReverse},
    {"remove", TestRemove},
    {NULL, NULL}
};
