#include "acutest.h"
#include "utils.h"

#include "calendar.h"


void RunTest(int number_of_days, enum Week start, const char* expected_file) {
    REDIRECT_IN_FILE(PrintCalendar(number_of_days, start));
    FilesCompare("log.txt", expected_file);
}


void TestCalendar() {
    RunTest(5, kFriday, "../../3-cycles/calendar/tests/5_Friday.txt");
    RunTest(7, kMonday, "../../3-cycles/calendar/tests/7_Monday.txt");
    RunTest(10, kFriday, "../../3-cycles/calendar/tests/10_Friday.txt");
    RunTest(10, kMonday, "../../3-cycles/calendar/tests/10_Monday.txt");
    RunTest(10, kSaturday, "../../3-cycles/calendar/tests/10_Saturday.txt");
    RunTest(10, kSunday, "../../3-cycles/calendar/tests/10_Sunday.txt");
    RunTest(10, kThursday, "../../3-cycles/calendar/tests/10_Thursday.txt");
    RunTest(10, kTuesday, "../../3-cycles/calendar/tests/10_Tuesday.txt");
    RunTest(10, kWednesday, "../../3-cycles/calendar/tests/10_Wednesday.txt");
    RunTest(19, kThursday, "../../3-cycles/calendar/tests/19_Thursday.txt");
    RunTest(27, kTuesday, "../../3-cycles/calendar/tests/27_Tuesday.txt");
    RunTest(47, kSunday, "../../3-cycles/calendar/tests/27_Sunday.txt");
    RunTest(99, kMonday, "../../3-cycles/calendar/tests/99_Monday.txt");
}


TEST_LIST = {
    {"calendar", TestCalendar},
    {NULL, NULL}
};
