#include "acutest.h"
#include "utils.h"

#include "standard.h"
#include "diagonal.h"
#include "ladder.h"
#include "zigzag.h"
#include "spiral.h"


static const int kInputRows[] = {2, 5, 3, 6, 8, 11};
static const int kInputColumns[] = {2, 5, 6, 3, 8, 11};


void RunTest(void (*order)(int, int), int rows, int columns, const char* expected_file) {
    REDIRECT_IN_FILE(order(rows, columns));
    FilesCompare("log.txt", expected_file);
}

void CommonTests(void (*function)(int, int)) {
    RunTest(function, 1, 1, "../../3-cycles/order/tests/1_1.txt");
    RunTest(function, 1, 5, "../../3-cycles/order/tests/1_5.txt");
    RunTest(function, 5, 1, "../../3-cycles/order/tests/5_1.txt");
}

void IndividualTests(void (*function)(int, int), const char* folder) {
    for (unsigned int i = 0; i < sizeof(kInputRows) / sizeof(int); ++i) {
        char path[100];
        int sizes = sprintf(path, "../../3-cycles/order/tests/%s/%d_%d.txt",
                folder, kInputRows[i], kInputColumns[i]);
        path[sizes] = '\0';
        RunTest(function, kInputRows[i], kInputColumns[i], path);
    }

}

void TestRows() {
    CommonTests(RowsOrder);
    IndividualTests(RowsOrder, "rows");
}

void TestColumns() {
    CommonTests(ColumnsOrder);
    IndividualTests(ColumnsOrder, "columns");
}

void TestSnake() {
    CommonTests(SnakeOrder);
    IndividualTests(SnakeOrder, "snake");
}

void TestDiagonal() {
    CommonTests(DiagonalOrder);
    IndividualTests(DiagonalOrder, "diagonal");
}

void TestLadder() {
    CommonTests(LadderOrder);
    IndividualTests(LadderOrder, "ladder");
}

void TestZigZag() {
    CommonTests(ZigZagOrder);
    IndividualTests(ZigZagOrder, "zigzag");
}

void TestSpiral() {
    CommonTests(SpiralOrder);
    IndividualTests(SpiralOrder, "spiral");
}


TEST_LIST = {
    {"rows", TestRows},
    {"columns", TestColumns},
    {"snake", TestSnake},
    {"diagonal", TestDiagonal},
    {"ladder", TestLadder},
    {"zigzag", TestZigZag},
    {"spiral", TestSpiral},
    {NULL, NULL}
};
