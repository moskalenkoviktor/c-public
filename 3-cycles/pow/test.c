#include "acutest.h"
#include "pow.h"


void TestSignature() {
    (void)((int (*)(int, long, int))Pow);
}


void TestSimple() {
    TEST_ASSERT(Pow(2, 3, 19) == 8);
    TEST_ASSERT(Pow(3, 0, 19) == 1);
    TEST_ASSERT(Pow(3, 3, 100) == 27);
    TEST_ASSERT(Pow(3, 4, 20) == 1);
    TEST_ASSERT(Pow(4, 8, 1000) == 536);
}


void TestHard() {
    TEST_ASSERT(Pow(1, 374834758345, 129237) == 1);
    TEST_ASSERT(Pow(2, 1000000000000000000, 1000000007) == 719476260);
    TEST_ASSERT(Pow(17239, 1000000000000000 - 1, 100000000) == 43181159);
    TEST_ASSERT(Pow(203042322, 82392839238824787, 92374) == 78360);
}


TEST_LIST = {
    {"simple", TestSimple},
    {"hard", TestHard},
    {NULL, NULL}
};
