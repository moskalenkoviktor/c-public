#include "acutest.h"
#include "sequence.h"
#include "utils.h"

void TestFirst(int a, int b, int c, double result) {
    TEST_ASSERT_(DoubleCompare(FirstSequence(a, b, c), result), "FirstSequence(%d, %d, %d) != %lf", a, b, c, result);
}

void TestSecond(unsigned int i, double result) {
    TEST_ASSERT_(DoubleCompare(SecondSequence(i), result), "SecondSequence(%d) != %lf", i, result);
}


void TestFirstSequence() {
    (void)((double (*)(int, int, unsigned int))FirstSequence);
    
    TestFirst(1, 1, 1, 1);
    TestFirst(1, 1, 2, 0);
    TestFirst(1, 1, 3, 1);
    TestFirst(1, 1, 4, 0);
    TestFirst(1, 1, 5, 1);
    TestFirst(1, 1, 6, 0);
    TestFirst(1, 1, 10, 0);
    TestFirst(1, 1, 11, 1);
    TestFirst(1, 1, 57, 1);

    TestFirst(2, 3, 1, 2);
    TestFirst(2, 3, 2, -1);
    TestFirst(2, 3, 3, -0.5);
    TestFirst(2, 3, 4, -0.833333);
    TestFirst(2, 3, 5, 5.166667);
    TestFirst(2, 3, 7, 10.166667);
    TestFirst(2, 3, 10, -25.972222);
    TestFirst(2, 3, 11, -17.972222);
    TestFirst(2, 3, 32, -41544.921532);

    TestFirst(10, 6, 11, -2600.048889);

    TestFirst(-5, -19, 1, -5);
    TestFirst(-5, -19, 4, 13.852632);
    TestFirst(-5, -19, 5, 108.852632);
    TestFirst(-5, -19, 6, 83.852632);
    TestFirst(-5, -19, 11, -8705.184598);
}

void TestSecondSequence() {
    TestSecond(0, 1);
    TestSecond(1, 2);
    TestSecond(2, 3);
    TestSecond(3, -2);
    TestSecond(4, -3);
    TestSecond(5, -2);
    TestSecond(6, -1);
    TestSecond(7, 2);
    TestSecond(8, 1);
    TestSecond(9, 2);
    TestSecond(10, 3);
    TestSecond(11, -2);
    TestSecond(12, -3);
    TestSecond(13, -2);
    TestSecond(14, -1);
    TestSecond(15, 2);
}


TEST_LIST = {
    {"test_first_sequence", TestFirstSequence},
    {"test_second_sequence", TestSecondSequence},
    {NULL, NULL}
};
