Реализуйте две функции `FirstSequence` и `SecondSequence`. 

Функция `FirstSequence` вычисляет сумму первых `count` слагаемых у следующего выражения: 


$$ a - b + \frac{1}{a} - \frac{1}{b} + ab - a^2 + b^2 - \frac{1}{a^2} + \frac{1}{b^2} - a^2b^2 + ... $$

Функция `SecondSequence` вычисляет `i`-ое число следующей [рекуррентной](https://ru.wikipedia.org/wiki/%D0%A0%D0%B5%D0%BA%D1%83%D1%80%D1%80%D0%B5%D0%BD%D1%82%D0%BD%D0%B0%D1%8F_%D1%84%D0%BE%D1%80%D0%BC%D1%83%D0%BB%D0%B0) формулы:


$$ a_0 = 1, a_1=2, a_2=3 $$

Для четных `i`:

$$ a_{i} = a_{i-1} - a_{i-2} + a_{i-3} $$

Для нечетных `i`:

$$ a_{i} = -a_{i-1} + a_{i-2} - a_{i-3} $$
