# Новогодняя ёлочка :christmas_tree: :christmas_tree: :christmas_tree: :christmas_tree: :christmas_tree:

Написуйте новогоднюю ёлочку в консоль. :tada: :tada: :tada:

Ёлочка состоит из уровней, каждый новый уровень по высоте больше на один, чем предыдущий. Ёлочка должна быть очень красивой и устойчивой, поэтому на макушке *сияющая звезда*, а снизу *надёжная подставка*. 


*Посмотрите файлы в папке `tests`, что от вас ожидается.*

## Примечание

Сначала реализуйте функцию, которая рисует каждый уровень отдельно (с заданным отступом). Функция печати ёлочки - это печать звезды, печать в цикле уровней с заданным отступом, затем печать подставки.
