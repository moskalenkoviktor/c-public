#include "acutest.h"
#include "utils.h"

#include "new-year.h"

static const int kInputLevels[] = {5, 2, 4, 3, 1};
static const int kInputH[] = {1, 2, 3, 5, 8};

void RunTest(int levels, int h_start, const char* expected_file) {
    REDIRECT_IN_FILE(ChristmasTree(levels, h_start));
    FilesCompare("log.txt", expected_file);
}

void TestChristmasTree() {
    for (unsigned int i = 0; i < sizeof(kInputLevels) / sizeof(int); ++i) {
        char path[100];
        int sizes = sprintf(path, "../../3-cycles/new-year/tests/%d_%d.txt", kInputLevels[i], kInputH[i]);
        path[sizes] = '\0';
        RunTest(kInputLevels[i], kInputH[i], path);
    }
}

TEST_LIST = {
    {"christmas", TestChristmasTree},
    {NULL, NULL}
};
