Вам дано число `current` типа `int` - закодированная дата. Последние четыре цифры - это год, далее две цифры на месяц, оставшиеся цифры (либо одна, либо две, ведущие нули отбрасываются) - день. Требуется реализовать функцию `next_day`, которая в таком же формате вернёт следующий календарный день. Считайте, что в переменой `current` содержится корректная дата.

## Подводные камни

1. Месяца имеют разное количество дней. Всё как в жизни, учитывайте это.
2. Год может быть високосным, февраль не всегда 28 дней.

## Примечание

Не нужно писать весь код в одной функции. В этой задаче у вас есть несколько независимых друг от друга фрагментов решения, и разумнее всего такие фрагменты выносить в отдельные функции. За качественный код буду награждать дополнительными баллами.

