import os
from argparse import ArgumentParser

def get_block_task(name: str) -> tuple[str, int]:
    if name == 'sum':
        return '0-begin', 0
    for block in os.listdir('.'):
        if not os.path.isdir(block):
            continue
        if name in os.listdir(block):
            return block, int(block[0])
    raise KeyError

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('task_name')
    args = parser.parse_args()
    try:
        dir_, number = get_block_task(args.task_name)
        print(f'{dir_} {number}')
    except KeyError:
        print('Task name incorrect!!!')
        exit(1)
