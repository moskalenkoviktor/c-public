#include <stdbool.h>
#include <math.h>

#include "acutest.h"

#define MAX_SIZE 100
#define REDIRECT_IN_FILE(function) { \
    FILE* saved = stdout; \
    stdout = fopen("log.txt", "w"); \
    \
    function; \
    \
    fclose(stdout); \
    stdout = saved; \
}


bool DoubleCompare(double first, double second) {
    return fabs(first - second) < 1e-6;
}

void TestLine(const char* actual, const char* expected) {
    TEST_ASSERT_(strcmp(actual, expected) == 0,
        "\nActual line: \n%s\nExpected line: \n%s\n",
        actual, expected);
}

void DeleteSpaces(char* buffer) {
    int end = strcspn(buffer, "\n");
    while (end > 0 && buffer[end - 1] == ' ') {
        buffer[end - 1] = '\n';
        buffer[end] = '\0';
        end--;
    }
}

void FilesCompare(const char* actual, const char* expected) {
    FILE* actual_file = fopen(actual, "r");
    FILE* expected_file = fopen(expected, "r");
    char actual_buffer[MAX_SIZE] = "\0";
    char expected_buffer[MAX_SIZE] = "\0";
    for (int i = 0; fgets(expected_buffer, MAX_SIZE, expected_file); ++i) {
        fgets(actual_buffer, MAX_SIZE, actual_file);
        TEST_CASE_("%s -> Line %d", expected, i + 1);
        DeleteSpaces(actual_buffer);
        DeleteSpaces(expected_buffer);
        TestLine(actual_buffer, expected_buffer);
    }
    TEST_CASE("number of lines");
    TEST_ASSERT_(fgets(actual_buffer, MAX_SIZE, actual_file) == NULL,
                       "too many lines");
    fclose(actual_file);
    fclose(expected_file);
}
