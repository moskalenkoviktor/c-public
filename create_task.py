from argparse import ArgumentParser
from pathlib import Path


def update_cmakelists(dir: Path, name: Path):
    with open(dir / 'CMakeLists.txt', 'a') as f:
        f.write(f'add_executable({name} {name}/test.c)\n')


def create_files(path: Path):
    path.mkdir(parents=True)
    with open(path / f'{path.name}.h', 'w') as f:
        f.write('int foo() {\n    return 0; // No implementation\n}\n')
    with open(path / 'test.c', 'w') as f:
        f.write('#include "acutest.h"\n\nTEST_LIST = {\n};')
    with open(path / 'README.md', 'w') as f:
        f.write('Тут должно быть условие задачи')


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('path')
    args = parser.parse_args()
    path = Path(args.path)
    create_files(path)
    update_cmakelists(path.parent, path.name)
